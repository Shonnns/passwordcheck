﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PasswordCheck;

namespace CheckPasswordConsole
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string password = Console.ReadLine();

            Check check = new Check(5, containsLowerCharts: true, containsSpecialCharts: true, containsUpperCharts: true);

            if (check.CheckPassword(password))
            {
                Console.WriteLine("Пароль соответсвует требованиям");
            }
            else
            {
                Console.WriteLine("Пароль не соответсвует требованиям");
            }

            Console.ReadKey();
        }
    }
}
