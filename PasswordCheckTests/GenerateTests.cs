﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PasswordCheck;

namespace PasswordCheckTests
{
    [TestClass]
    public class GenerateTests
    {
        [TestMethod]
        public void LowerCaseCharts_allLowerCharts_currenct()
        {
            // Arrange
            string allCharts = "abcdefghijklmnopqrstuvwxyz";

            // Act
            Generate generate = new Generate();

            string actual = generate.LowerCaseCharts();

            // Assert
            Assert.AreEqual(allCharts, actual);
        }
    }
}
