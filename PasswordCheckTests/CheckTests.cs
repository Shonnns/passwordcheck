﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PasswordCheck;

namespace PasswordCheckTests
{
    [TestClass]
    public class CheckTests
    {
        [TestMethod]
        public void CurrectMinLength_password5chartsAnd5chartsSettings_true()
        {
            // Arrange
            int minLength = 5;
            string password = "abcdf";

            // Act
            Check check = new Check(minLength);

            bool actual = check.CurrectMinLength(password);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void CurrectMinLength_password5chartsAnd10chartsSettings_false()
        {
            // Arrange
            int minLength = 10;
            string password = "abcdf";

            // Act
            Check check = new Check(minLength);

            bool actual = check.CurrectMinLength(password);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void CurrectMinLength_password20chartsAnd15chartsSettings_true()
        {
            // Arrange
            int minLength = 15;
            string password = "abcdfdsadsaadsassdsa";

            // Act
            Check check = new Check(minLength);

            bool actual = check.CurrectMinLength(password);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void CurrectUpperCharts_paswordContainUpperCharts_true()
        {
            // Arrange
            bool UpperCharts = true;
            string password = "AfvnASDvas";

            // Act
            Check check = new Check(containsUpperCharts: UpperCharts); ;

            bool actual = check.CurrectUpperCharts(password);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void CurrectSpecialCharts_passwordContainSpecialCharts_true()
        {
            // Arrange
            bool SpecialCharts = true;
            string password = "sdfd@sdf5!";

            // Act
            Check check = new Check(containsSpecialCharts: SpecialCharts);

            bool actual = check.CurrectSpecialCharts(password);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void CurrectSpecialCharts_passwordNotContainSpecialCharts_false()
        {
            // Arrange
            bool SpecialCharts = true;
            string password = "sdfdsdf5fds";

            // Act
            Check check = new Check(containsSpecialCharts: SpecialCharts);

            bool actual = check.CurrectSpecialCharts(password);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void CheckPassword_CurrectPasswordAndAllSettings_true()
        {
            // Arrange
            string password = "fAfd!@hvdc";


            // Act
            Check check = new Check(containsLowerCharts: true, containsSpecialCharts: true, containsUpperCharts: true);

            bool actual = check.CheckPassword(password);

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void CheckPassword_ShortPasswordAndAllSettings_false()
        {
            // Arrange
            string password = "fA!";


            // Act
            Check check = new Check(containsLowerCharts: true, containsSpecialCharts: true, containsUpperCharts: true);

            bool actual = check.CheckPassword(password);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void CheckPassword_onlyLowerPasswordAndAllSettings_false()
        {
            // Arrange
            string password = "asfghj!";


            // Act
            Check check = new Check(containsLowerCharts: true, containsSpecialCharts: true, containsUpperCharts: true);

            bool actual = check.CheckPassword(password);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void CheckPassword_onlyUpperPasswordAndAllSettings_false()
        {
            // Arrange
            string password = "ADFSFF!";


            // Act
            Check check = new Check(containsLowerCharts: true, containsSpecialCharts: true, containsUpperCharts: true);

            bool actual = check.CheckPassword(password);

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void CheckPassword_NotSpecialChartsPasswordAndAllSettings_false()
        {
            // Arrange
            string password = "AdfasSADf";


            // Act
            Check check = new Check(containsLowerCharts: true, containsSpecialCharts: true, containsUpperCharts: true);

            bool actual = check.CheckPassword(password);

            // Assert
            Assert.IsFalse(actual);
        }
    }
}
