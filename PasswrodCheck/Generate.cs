﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PasswordCheck
{
    public class Generate
    {
        // минимальная длина генерируемого пароля
        private int MinLength = 5;

        // добавлять ли символы в верхнем регистре
        private bool ContainsUpperCharts = false;

        // добавлять ли символы в нижнем регистре
        private bool ContainsLowerCharts = false;

        // добавлять ли специальные символы
        private bool ContainsSpecialCharts = false;

        // Класс для проверки пароля
        private Check Check;

        private char[] charts;

        private string specialCharts = "!@?<>#";

        public Generate(int minLength = 5,
                 bool containsUpperCharts = false,
                 bool containsLowerCharts = false,
                 bool containsSpecialCharts = false)
        {
            this.MinLength = minLength;
            this.ContainsUpperCharts = containsUpperCharts;
            this.ContainsLowerCharts = containsLowerCharts;
            this.ContainsSpecialCharts = containsSpecialCharts;

            // Объявление класса проверки пароля
            Check = new Check(minLength, containsUpperCharts, containsLowerCharts, containsSpecialCharts);

            this.charts = GenerateCharts();
        }

        // Генерация нового пароля по условию
        public string GeneratePassword()
        {
            string newPassword = "";

            Random random = new Random();

            do
            {
                for (int i = 0; i < MinLength; i++)
                {
                    newPassword += charts[random.Next(0, charts.Length)];
                }
            } while (!Check.CheckPassword(newPassword));

            return newPassword;
        }

        // Генерация алфавита символов
        public char[] GenerateCharts()
        {
            string finalCharts = "";

            if(this.ContainsLowerCharts)
            {
                finalCharts += LowerCaseCharts();
            }

            if (this.ContainsUpperCharts)
            {
                finalCharts += UpperCaseCharts();
            }

            if (this.ContainsSpecialCharts)
            {
                finalCharts += specialCharts;
            }

            return finalCharts.ToCharArray();
        }

        // Сформировать сторку символов в нижнем регистре
        public string LowerCaseCharts()
        {
            string lower = "";
            for (int i = 97; i <= 122; i++)
            {
                lower += (char)i;
            }

           return lower;
        }

        // Сформировать сторку символов в верхнем регистре
        public string UpperCaseCharts()
        {
            string upper = "";
            for (int i = 65; i <= 90; i++)
            {
                upper += (char)i;
            }

            return upper;
        }
    }
}
