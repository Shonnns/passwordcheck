﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PasswordCheck
{
    public class Check
    {
        // минимальная длина пароля
        private int MinLength = 5;

        // проверять ли символы в верхнем регистре
        private bool ContainsUpperCharts = false;

        // проверять ли символы в нижнем регистре
        private bool ContainsLowerCharts = false;

        // проверять ли наличие специальных символов
        private bool ContainsSpecialCharts = false;

        // коструктор для настройки первоначальных параметров проверки пароля
        public Check(int minLength = 5,
                       bool containsUpperCharts = false,
                       bool containsLowerCharts = false,
                       bool containsSpecialCharts = false)
        {
            this.MinLength = minLength;
            this.ContainsUpperCharts = containsUpperCharts;
            this.ContainsLowerCharts = containsLowerCharts;
            this.ContainsSpecialCharts = containsSpecialCharts;
        }

        // Проверка пароля
        public bool CheckPassword(string password)
        {
            return CurrectMinLength(password)
                && CurrectUpperCharts(password)
                && CurrectLoverCharts(password)
                && CurrectSpecialCharts(password);
        }

        // Проверка длины пароля
        public bool CurrectMinLength(string password)
        {
            return password.Length >= MinLength;
        }

        // Проверка наличия символов в верхнем регистре
        public bool CurrectUpperCharts(string password)
        {
            return !this.ContainsUpperCharts || password.ToLower() != password;
        }

        // Проверка наличия символов в нижнем регистре
        public bool CurrectLoverCharts(string password)
        {
            return !this.ContainsLowerCharts || password.ToUpper() != password;
        }

        // Наличие специальных символов
        public bool CurrectSpecialCharts(string password)
        {
            char[] specialChars = { '!', '@', '?', '<', '>', '#' };

            if (!this.ContainsSpecialCharts)
            {
                return true;
            }

            foreach (var chart in specialChars)
            {
                if (password.Contains(chart))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
